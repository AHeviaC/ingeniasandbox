#pragma once

class Movil
{
public:
	// Posicion del movil
	double x, y;

	Movil(double _x, double _y);
	virtual ~Movil(void);

	virtual void simular(double inc_t);
};

