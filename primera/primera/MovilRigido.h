#pragma once

#include "Movil.h"

class MovilRigido : public Movil
{
public:
	// Constructor / Destructor
	MovilRigido(void);
	virtual ~MovilRigido(void);

	// M�todos
	virtual void simular(double inc_t);

};

