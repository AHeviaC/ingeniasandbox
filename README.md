# README #

Este es un repositorio de pruebas. NO subir nada que sea definitivo. Lo más probable es que lo borremos!!

### Pruebas de Fork y Pull Request ###

Ver ayuda en:

https://www.atlassian.com/git/tutorials/making-a-pull-request/example

Vamos a usar el flujo de trabajo que aparece en el ejemplo *"Example"* entre "Mary" y "John", donde "Mary" serán los alumnos y "John" los profesores. Es muy posible, incluso seguro, que los coordinadores sean los únicos que puedan hacer los *pull requests*.

### Flujo de trabajo ###

El flujo definitivo de trabajo lo indicaremos claramente en el repositorio definitivo. 